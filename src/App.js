import React, { useState } from "react";
import "./App.css";
import pdfjsLib from "pdfjs-dist/webpack";
const App = () => {
  //const [a, setA] = useState("");

  const changeHandler = async (event) => {
    const file = event.target.files[0];
    const uri = URL.createObjectURL(file);
    let loadingTask = await pdfjsLib.getDocument({ url: uri });
    let imageArray = [];
    for (let i = 1; i <= loadingTask.numPages; i++) {
      var currentPage = i;
      var page = await loadingTask.getPage(currentPage);
      var viewport = page.getViewport(currentPage);
      var render_context = {
        canvasContext: document.querySelector("#pdf-canvas").getContext("2d"),
        viewport: viewport,
      };
      await page.render(render_context);
      var canvas = document.getElementById("pdf-canvas");
      var img = canvas.toDataURL("image/png");
      imageArray.push(img);
    }
    console.log(imageArray);
  };
  return (
    <div className="App">
      <div className="App-body">
        <input type="file" name="file" onChange={changeHandler} />
        <canvas style={{ display: "none" }} id="pdf-canvas"></canvas>
      </div>
    </div>
  );
};

export default App;
